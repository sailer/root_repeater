#include <cstdlib>
#include <iostream>
#include <string>
#include <dlfcn.h>

int main(int nargs, char* argv[]) {
  auto aLib = argv[1];
  if( ! dlopen(aLib, RTLD_LAZY | RTLD_LOCAL ) ) {
      std::cerr << "ERROR: failed to load " << aLib << ": " << dlerror() << std::endl;
      return 1;
  }
  std::cout << "Library " << aLib << " loaded successfully" << std::endl;
  return 0;
}
